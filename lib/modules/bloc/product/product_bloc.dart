import 'package:flutter_playground_kodingworks/modules/model/category-model.dart';
import 'package:flutter_playground_kodingworks/modules/model/product-model.dart';
import 'package:flutter_playground_kodingworks/modules/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class ProductBloc {
  final repository = Repository();
  final fetcher = PublishSubject<List<Product>>();

  Observable<List<Product>> get productList => fetcher.stream;

  fetchProductList() async {
    List<Product> itemModel = await repository.fetchAllProduct();
    fetcher.sink.add(itemModel);
  }
  dispose() {
    fetcher.close();
  }
}

final productBloc = ProductBloc();