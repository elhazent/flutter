import 'package:flutter_playground_kodingworks/modules/model/category-model.dart';
import 'package:flutter_playground_kodingworks/modules/model/product-model.dart';
import 'package:flutter_playground_kodingworks/modules/model/slidebanner-model.dart';
import 'package:flutter_playground_kodingworks/modules/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class BannerBloc {
  final repository = Repository();
  final fetcher = PublishSubject<List<SlideBanner>>();

  Observable<List<SlideBanner>> get bannerList => fetcher.stream;

  fetchBannerList() async {
    List<SlideBanner> itemModel = await repository.fetchAllBanner();
    fetcher.sink.add(itemModel);
  }
  dispose() {
    fetcher.close();
  }
}

final bannerBloc = BannerBloc();