import 'package:flutter_playground_kodingworks/modules/model/category-model.dart';
import 'package:flutter_playground_kodingworks/modules/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class CategoryBloc {
  final repository = Repository();
  final fetcher = PublishSubject<List<Category>>();

  Observable<List<Category>> get categoryList => fetcher.stream;

  fetchCategoryList() async {
    List<Category> itemModel = await repository.fetchAllCategory();
    fetcher.sink.add(itemModel);
  }
  dispose() {
    fetcher.close();
  }
}

final categoryBloc = CategoryBloc();