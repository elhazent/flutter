//part of 'catalog_bloc.dart';
//
//abstract class CatalogState extends Equatable {
//  const CatalogState();
//}
//
//class CatalogInitial extends CatalogState {
//  @override
//  List<Object> get props => [];
//}
//
//class CategoryFetchSuccess extends CatalogState {
//  final List<Category> category;
//
//  const CategoryFetchSuccess({this.category});
//
//  @override
//  List<Object> get props => [category];
//}
//
//class CategoryFetchError extends CatalogState {
//  final String error;
//
//  const CategoryFetchError({this.error});
//
//  @override
//  List<Object> get props => [error];
//
//  @override
//  String toString() => 'GET { error: $error }';
//}
//
//class ProductFetchSuccess extends CatalogState {
//  final List<Product> product;
//
//  const ProductFetchSuccess({this.product});
//
//  @override
//  List<Object> get props => [product];
//}
//
//class ProductFetchError extends CatalogState {
//  final String error;
//
//  const ProductFetchError({this.error});
//
//  @override
//  List<Object> get props => [error];
//
//  @override
//  String toString() => 'GET { error: $error }';
//}