import 'package:dio/dio.dart';
import 'package:flutter_playground_kodingworks/modules/model/category-model.dart';
import 'package:flutter_playground_kodingworks/modules/model/product-model.dart';
import 'package:flutter_playground_kodingworks/modules/model/slidebanner-model.dart';
import 'package:http/http.dart';

class ApiProvider{
  Client client = Client();
  Dio dio = Dio();


  Future<List<Category>> fetchCategory() async {
    try {
      final response = await dio.get("https://api.tumbasin.id/v1/products/categories");
      return (response.data as List).map((p) => Category.fromJson(p)).toList();
    } catch (e) {
      return e;
    }
  }
  Future<List<Product>> fetchProduct() async {
    try {
      final response = await dio.get("https://api.tumbasin.id/v1/products?vendor=1000&featured=true");
      return (response.data as List).map((p) => Product.fromJson(p)).toList();
    } catch (e) {
      return e;
    }
  }
  Future<List<SlideBanner>> fetchBanner() async {
    try {
      final response = await dio.get("https://dashboard.tumbasin.id/wp-json/wp/v2/media?media_category[]=39");
      return (response.data as List).map((p) => SlideBanner.fromJson(p)).toList();
    } catch (e) {
      return e;
    }
  }
}