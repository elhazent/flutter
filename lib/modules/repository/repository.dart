import 'package:flutter_playground_kodingworks/modules/model/category-model.dart';
import 'package:flutter_playground_kodingworks/modules/model/product-model.dart';
import 'package:flutter_playground_kodingworks/modules/model/slidebanner-model.dart';
import 'package:flutter_playground_kodingworks/modules/provider/api_provider.dart';

class Repository {
  final apiProvider = ApiProvider();
  Future<List<Category>> fetchAllCategory() => apiProvider.fetchCategory();
  Future<List<Product>> fetchAllProduct() => apiProvider.fetchProduct();
  Future<List<SlideBanner>> fetchAllBanner() => apiProvider.fetchBanner();

}