import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_playground_kodingworks/feature/tools/shimmer.dart';
import 'package:flutter_playground_kodingworks/feature/tools/sliver_appbar.dart';
import 'package:flutter_playground_kodingworks/modules/bloc/banner/banner_bloc.dart';
import 'package:flutter_playground_kodingworks/modules/bloc/category/category_bloc.dart';
import 'package:flutter_playground_kodingworks/modules/bloc/product/product_bloc.dart';
import 'package:flutter_playground_kodingworks/modules/model/category-model.dart';
import 'package:flutter_playground_kodingworks/modules/model/product-model.dart';
import 'package:flutter_playground_kodingworks/modules/model/slidebanner-model.dart';
import 'package:flutter_playground_kodingworks/shared/colors.dart';
import 'package:flutter_playground_kodingworks/shared/helpers/format_currency.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sliver_fill_remaining_box_adapter/sliver_fill_remaining_box_adapter.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController controller;
  List<bool> isCart = [];
  List<int> quantity = [];
  List<int> price = [];
  int totalprice = 0;
  int _current = 0;
  int totalitem = 0;
  @override
  Widget build(BuildContext context) {
    productBloc.fetchProductList();
    categoryBloc.fetchCategoryList();
    bannerBloc.fetchBannerList();
    print("width : ${MediaQuery.of(context).size.width}");
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: CustomScrollView(
          slivers: <Widget>[
            SliverPersistentHeader(
              delegate: MySliverAppBar(expandedHeight: 150* (MediaQuery.of(context).size.width / 411)),
              pinned: true,
              floating: true,
            ),
            SliverFillRemainingBoxAdapter(
              child: Container(
                color: Colors.white,
                alignment: Alignment.topCenter,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 60,),
                      Text("Telusuri Jenis Product",style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold
                      ),),
                      StreamBuilder(
                        stream: categoryBloc.categoryList,
                        builder: (context,  AsyncSnapshot<List<Category>> snapshot){
                          if (snapshot.hasData) {
                            return Container(
                              height: 200 * (MediaQuery.of(context).size.width / 411),
                              width: MediaQuery.of(context).size.width - 40,
                              child: GridView.builder(
                                itemCount: snapshot.data.length,
                                physics: NeverScrollableScrollPhysics(),
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount:5,mainAxisSpacing: 10* (MediaQuery.of(context).size.width / 411)),
                                itemBuilder: (BuildContext context, int index) {
                                  return Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  decoration: BoxDecoration(
                                                      color: Color(0xffFDF4FC),
                                                      borderRadius:
                                                      BorderRadius.circular(10* (MediaQuery.of(context).size.width / 411)),
                                                      boxShadow: [
                                                        BoxShadow(color: Colors.grey, blurRadius: 2 * (MediaQuery.of(context).size.width / 411) ?? 0)
                                                      ]),
                                                  child: Material(
                                                    color: Colors.transparent,
                                                    child: InkWell(
                                                      onTap: () {},
                                                      child: Container(
                                                        padding:EdgeInsets.all(8),
                                                        child: Image.network("${snapshot.data[index].image.src}",width: 30* (MediaQuery.of(context).size.width / 411),height: 30* (MediaQuery.of(context).size.width / 411),),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(height: 5* (MediaQuery.of(context).size.width / 411),),
                                                Text(
                                                    "${snapshot.data[index].name}",
                                                    textAlign: TextAlign.center,
                                                    style:
                                                    TextStyle(color: Colors.black)

                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  );
                                },
                              ) ,
                            );
                          } else if (snapshot.hasError) {
                            return Text(snapshot.error.toString());
                          }
                          return Container(
                            height: 200 * (MediaQuery.of(context).size.width / 411),
                            width: MediaQuery.of(context).size.width - 40,
                            child: ShimmerList(),
                          );

                        },
                      ),
                      StreamBuilder(
                        stream: bannerBloc.bannerList,
                        builder: (context,  AsyncSnapshot<List<SlideBanner>> snapshot){
                          if (snapshot.hasData) {
                            final List<String> imgList = [];
                            for (int i = 0; i < snapshot.data.length; i++){
                              imgList.add("${snapshot.data[i].mediaDetails.sizes.medium.s3.url}");
                            }
                            return CarouselSlider(
                              options: CarouselOptions(
                                autoPlay: true,
                                aspectRatio: 2.5,
                                enlargeCenterPage: true,
                                initialPage: 0,
                                reverse: false,
                                enableInfiniteScroll: true,
                                autoPlayInterval: Duration(seconds: 4),
                                autoPlayAnimationDuration: Duration(milliseconds: 2000),
                                onPageChanged: (index, position) {
                                  setState(() {
                                    _current = index;
                                  });
                                },
                                scrollDirection: Axis.horizontal,
                              ),
                              items: imgList.map((imgUrl) {
                                return Builder(
                                  builder: (BuildContext context) {
                                    return Container(
                                      width: MediaQuery.of(context).size.width,
                                      height: 300 * (MediaQuery.of(context).size.width / 411),
                                      margin: EdgeInsets.symmetric(horizontal: 10.0),
                                      decoration: BoxDecoration(
//                              color: Colors.green,
                                      ),
                                      child: Image.network(
                                        imgUrl,
                                        fit: BoxFit.fill,
                                      ),
                                    );
                                  },
                                );
                              }).toList(),
                            );
                          } else if (snapshot.hasError) {
                            return Text(snapshot.error.toString());
                          }
                          return Container(
                              child: Shimmer.fromColors(
                                highlightColor: Colors.white,
                                baseColor: Colors.grey[300],
                                child: ShimmerBanner(),
                                period: Duration(seconds: 3),
                              )
                          );

                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Container(
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("Product Terlaris",style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold
                                ),),
                                Text("Lihat Semua",style: TextStyle(
                                    color: BaseColor.primary
                                ),)
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      StreamBuilder(
                        stream: productBloc.productList,
                        builder: (context,  AsyncSnapshot<List<Product>> snapshot){
                          if (snapshot.hasData) {
                            return Container(
                              height: 105  * snapshot.data.length.toDouble(),
                              child: ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: snapshot.data.length,
                                itemBuilder: (context,position){
                                  if (snapshot.hasData) {
                                    if (isCart.length < snapshot.data.length){
                                      isCart.add(false);
                                      quantity.add(0);
                                      price.add(0);
                                      print("cart : ${isCart.length} ${snapshot.data.length} $position");
                                    }
                                    price[position] = int.parse(snapshot.data[position].price);

                                    return  Container(
                                      alignment: Alignment.topCenter,
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            child: Row(
                                              children: <Widget>[
                                                Container(
                                                  decoration: BoxDecoration(
                                                      borderRadius:  BorderRadius.all(Radius.circular(5)), boxShadow: [
                                                    BoxShadow(color: Colors.grey, blurRadius: 2 * (MediaQuery.of(context).size.width / 411) ?? 0)
                                                  ]
                                                  ),
                                                  child: ClipRRect(
                                                    child: Image.network("${snapshot.data[position].images[0].src}",width: 70* (MediaQuery.of(context).size.width / 411),height: 70* (MediaQuery.of(context).size.width / 411),),
                                                    borderRadius: BorderRadius.all(Radius.circular(5)),
                                                  ),
                                                ),
                                                SizedBox(width: 15,),
                                                Expanded(
                                                  child: Container(
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: <Widget>[
                                                        Text("${snapshot.data[position].name}"),
                                                        SizedBox(height: 10,),
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                          children: <Widget>[
                                                            RichText(
                                                              text: TextSpan(
                                                                text: '${priceFormat(double.parse(snapshot.data[position].price))} ',
                                                                style: TextStyle(color: BaseColor.primary),
                                                                children: <TextSpan>[
                                                                  TextSpan(text: '/${snapshot.data[position].metaData[0].value}', style: TextStyle(color: Colors.grey)),
                                                                ],
                                                              ),
                                                            ),
                                                            isCart[position] == true?Row(
                                                              children: <Widget>[
                                                                InkWell(
                                                                  onTap:(){
                                                                    quantity[position] = quantity[position]-1;
                                                                    totalprice = totalprice - (price[position]);
                                                                    if (quantity[position] == 0){
                                                                      isCart[position] = false;
                                                                      totalitem = totalitem - 1;
                                                                    }
                                                                    print("totalPrice : $totalprice $totalitem");

                                                                  },
                                                                  child: Container(
                                                                    decoration: BoxDecoration(
                                                                        borderRadius: BorderRadius.all(Radius.circular(5)),
                                                                        border: Border.all(color: Colors.grey)
                                                                    ),
                                                                    padding: EdgeInsets.all(5),
                                                                    child: Icon(
                                                                      Icons.minimize,
                                                                      size: 15,
                                                                      color: Colors.grey,
                                                                    ),
                                                                  ),
                                                                ),
                                                                Container(
                                                                    padding: EdgeInsets.symmetric(horizontal: 5),
                                                                    child: Text("${quantity[position]}")
                                                                ),
                                                                InkWell(
                                                                  onTap:(){
                                                                    quantity[position] = quantity[position]+1;
                                                                    totalprice = totalprice + (price[position]);
                                                                    print("totalPrice : $totalprice");
                                                                  },
                                                                  child: Container(
                                                                    decoration: BoxDecoration(
                                                                        borderRadius: BorderRadius.all(Radius.circular(5)),
                                                                        color: BaseColor.primary
                                                                    ),
                                                                    padding: EdgeInsets.all(5),
                                                                    child: Icon(
                                                                      Icons.add,
                                                                      size: 15,
                                                                      color: Colors.white,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ):InkWell(
                                                              onTap: (){
                                                                isCart[position] = true;
                                                                quantity[position] = quantity[position]+1;
                                                                totalprice = totalprice + price[position];
                                                                totalitem = totalitem + 1;
                                                                print("add click ${quantity[position]}  ${isCart[position]} $position $totalprice");
                                                                print("totalPrice : $totalprice $totalitem");
                                                                print("harga : ${snapshot.data[position].price}");
                                                              },
                                                              child: Container(
                                                                decoration: BoxDecoration(
                                                                    color: BaseColor.primary,
                                                                    borderRadius: BorderRadius.circular(3)
                                                                ),
                                                                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                                                                child: Text("Tambahkan",style: TextStyle(
                                                                    color: Colors.white
                                                                ),),
                                                              ),
                                                            )
                                                          ],
                                                        ),

                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Divider(),
                                        ],
                                      ),
                                    );
                                  } else if (snapshot.hasError) {
                                    return Text(snapshot.error.toString());
                                  }
                                  return Container(
                                      padding: EdgeInsets.symmetric(horizontal: 16),
                                      child: Shimmer.fromColors(
                                        highlightColor: Colors.white,
                                        baseColor: Colors.grey[300],
                                        child: ShimmerProduct(),
                                        period: Duration(seconds: 3),
                                      )
                                  );
                                },
                              ),
                            );
                          } else if (snapshot.hasError) {
                            return Text(snapshot.error.toString());
                          }
                          return Container(
                              padding: EdgeInsets.symmetric(horizontal: 16),
                              child: Shimmer.fromColors(
                                highlightColor: Colors.white,
                                baseColor: Colors.grey[300],
                                child: ShimmerProduct(),
                                period: Duration(seconds: 3),
                              )
                          );

                        },
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: totalitem != 0?FloatingActionButton.extended(
        onPressed: () {},
        backgroundColor: BaseColor.primary,
        label: Container(
          width: MediaQuery.of(context).size.width - 80,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text("${totalitem} item"),
                      Text(" | "),
                      Text("${priceFormat(totalprice.toDouble())}")
                    ],
                  ),
                  Text("Pasar Karangayu")
                ],
              ),
              Icon(
                Icons.shopping_cart,
              )
            ],
          ),
        ),
      ):FloatingActionButton(
        onPressed: () {},
        backgroundColor: BaseColor.primary,
        child: Icon(Icons.shopping_cart),
      ),
    );
  }

}
