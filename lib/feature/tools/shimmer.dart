import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int offset = 0;
    int time = 800;

    return SafeArea(
      child: GridView.builder(
        itemCount: 6,
        physics: NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount:5,mainAxisSpacing: 10* (MediaQuery.of(context).size.width / 411)),
        itemBuilder: (BuildContext context, int index) {
          offset += 5;
          time = 800 + offset;

          print(time);

          return Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Shimmer.fromColors(
                highlightColor: Colors.white,
                baseColor: Colors.grey[300],
                child: ShimmerCategory(),
                period: Duration(milliseconds: time),
              ));
        },
      ),
    );
  }
}

class ShimmerProduct extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double containerWidth = 230 * (MediaQuery.of(context).size.width / 411);
    double containerHeight = 15;

    return Container(
      margin: EdgeInsets.symmetric(vertical: 7.5),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            height: 70* (MediaQuery.of(context).size.width / 411),
            width: 70* (MediaQuery.of(context).size.width / 411),
            color: Colors.grey,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: containerHeight,
                width: containerWidth,
                color: Colors.grey,
              ),
              SizedBox(height: 5),
              Container(
                height: containerHeight,
                width: containerWidth,
                color: Colors.grey,
              ),
              SizedBox(height: 5),
              Container(
                height: containerHeight,
                width: containerWidth * 0.75,
                color: Colors.grey,
              )
            ],
          )
        ],
      ),
    );
  }
}
class ShimmerCategory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double containerWidth = 230 * (MediaQuery.of(context).size.width / 411);
    double containerHeight = 10;

    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: Column(
                children: <Widget>[
                  Container(
                    height: 50* (MediaQuery.of(context).size.width / 411),
                    width: 60* (MediaQuery.of(context).size.width / 411),
                    color: Colors.grey,
                  ),
                  SizedBox(height: 5* (MediaQuery.of(context).size.width / 411),),
                  Container(
                    height: containerHeight,
                    width: 60* (MediaQuery.of(context).size.width / 411),
                    color: Colors.grey,
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
class ShimmerBanner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double containerWidth = 230 * (MediaQuery.of(context).size.width / 411);
    double containerHeight = 10;

    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width - 80,
        height: 100,
        color: Colors.grey,
      ),
    );
  }
}