# flutter_playground_kodingworks
Flutter Playground KodingWorks

## Getting Started
Run this command to install dependencies:
- flutter pub get

Reference architecture :
- [Cookbook BLoc: BLoc state management](https://atomicdesign.bradfrost.com/)
- [Cookbook Atomic: Ui paradigm design](https://bloclibrary.dev/#/gettingstarted)